---
tags:
- generated_from_trainer
datasets:
- ncbi_disease
metrics:
- precision
- recall
- f1
- accuracy
model-index:
- name: biobert-base-cased-v1.2_ncbi_disease-softmax-labelall-ner
  results:
  - task:
      name: Token Classification
      type: token-classification
    dataset:
      name: ncbi_disease
      type: ncbi_disease
      args: ncbi_disease
    metrics:
    - name: Precision
      type: precision
      value: 0.8288508557457213
    - name: Recall
      type: recall
      value: 0.8614993646759848
    - name: F1
      type: f1
      value: 0.8448598130841122
    - name: Accuracy
      type: accuracy
      value: 0.9861487755016897
---

<!-- This model card has been generated automatically according to the information the Trainer had access to. You
should probably proofread and complete it, then remove this comment. -->

# biobert-base-cased-v1.2_ncbi_disease-softmax-labelall-ner

This model is a fine-tuned version of [dmis-lab/biobert-base-cased-v1.2](https://huggingface.co/dmis-lab/biobert-base-cased-v1.2) on the ncbi_disease dataset.
It achieves the following results on the evaluation set:
- Loss: 0.0629
- Precision: 0.8289
- Recall: 0.8615
- F1: 0.8449
- Accuracy: 0.9861

## Model description

More information needed

## Intended uses & limitations

More information needed

## Training and evaluation data

More information needed

## Training procedure

### Training hyperparameters

The following hyperparameters were used during training:
- learning_rate: 2e-05
- train_batch_size: 4
- eval_batch_size: 4
- seed: 42
- optimizer: Adam with betas=(0.9,0.999) and epsilon=1e-08
- lr_scheduler_type: linear
- lr_scheduler_warmup_ratio: 0.1
- num_epochs: 3

### Training results

| Training Loss | Epoch | Step | Validation Loss | Precision | Recall | F1     | Accuracy |
|:-------------:|:-----:|:----:|:---------------:|:---------:|:------:|:------:|:--------:|
| 0.0554        | 1.0   | 1359 | 0.0659          | 0.7814    | 0.8132 | 0.7970 | 0.9825   |
| 0.0297        | 2.0   | 2718 | 0.0445          | 0.8284    | 0.8895 | 0.8578 | 0.9876   |
| 0.0075        | 3.0   | 4077 | 0.0629          | 0.8289    | 0.8615 | 0.8449 | 0.9861   |


### Framework versions

- Transformers 4.18.0
- Pytorch 1.10.2+cu102
- Datasets 2.3.2
- Tokenizers 0.12.1
